<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicnicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('picnics', function(Blueprint $table)
		{
			$table->increments('picnic_id');
			$table->string('picnic_name');
    		$table->integer('picnic_taste_level'); // how tasty is this picnic?
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('picnics');
	}

}
