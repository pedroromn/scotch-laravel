<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBearsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bears', function(Blueprint $table)
		{
			$table->increments('bear_id');
            $table->string('bear_name');
            $table->string('bear_type');
            $table->integer('bear_danger_level'); //this will be between 1-10  
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bears');
	}

}
